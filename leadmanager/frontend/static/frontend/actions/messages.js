export const errorMessage = (msg) => {
  document.querySelector('alerts-component').show(msg, 'error')
}
export const successMessage = (msg) => {
  document.querySelector('alerts-component').show(msg, 'success')
}

export const infoMessage = (msg) => {
  document.querySelector('alerts-component').show(msg, 'info')
}
