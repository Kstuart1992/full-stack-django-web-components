import { Alerts } from "./layout/Alerts.js"
import { Header } from "./layout/Header.js"
import { Dashboard } from "./leads/Dashboard.js"

import store from "../store.js"

const template = document.getElementById("t-app")

class App extends HTMLElement {
  constructor() {
    super()
    this.appendChild(template.content.cloneNode(true))
  }
}

customElements.define("app-component", App)

export { App }
