import { deleteLead, getLeads } from "../../actions/leads.js"

const template = document.getElementById("t-leads")

class Leads extends HTMLElement {
  constructor() {
    super()
    this.appendChild(template.content.cloneNode(true))
    this.getLeads = getLeads
    this._leads = []
  }
  get leads() {
    return this._leads
  }

  set leads(newV) {
    this._leads = newV
  }

  async connectedCallback() {
    this.update()
  }

  onDelete(id) {
    console.log(id)
    this.leads = this.leads.filter((lead) => lead.id !== id)
    this.querySelector(`tr[key="${id}"]`).remove()
  }

  async update() {
    this.leads = await this.getLeads()
    this.leads
      .filter((lead) => !this.querySelector(`tr[key="${lead.id}"]`))
      .map((lead) => {
        let tr = document.createElement("tr")
        tr.setAttribute("key", lead.id)
        let tdId = document.createElement("td")
        let tdName = document.createElement("td")
        let tdEmail = document.createElement("td")
        let tdMessage = document.createElement("td")
        let tdDelete = document.createElement("td")
        let delButton = document.createElement("button")

        tdId.textContent = lead.id
        tdName.textContent = lead.name
        tdEmail.textContent = lead.email
        tdMessage.textContent = lead.message
        delButton.className = "btn btn-danger btn-sm"
        delButton.textContent = "Delete"

        delButton.onclick = deleteLead.bind(
          this,
          lead.id,
          this.onDelete.bind(this)
        )

        tdDelete.append(delButton)
        tr.append(tdId, tdName, tdEmail, tdMessage, tdDelete)
        this.querySelector("tbody").appendChild(tr)
      })
  }
}

customElements.define("leads-component", Leads)

export { Leads }
