import { addLead } from "../../actions/leads.js"

const template = document.getElementById("t-form")

class Form extends HTMLElement {
  constructor() {
    super()
    this.appendChild(template.content.cloneNode(true))
    this.state = { name: "", email: "", message: "" }
    this.state.reset = () => {
      this.state = { name: "", email: "", message: "" }
    }
    this.form = this.querySelector("form")
  }

  connectedCallback() {
    this.form.onsubmit = this.onSubmit.bind(this)

    this.querySelectorAll("form input,textarea").forEach((input) => {
      input.onchange = this.onChange.bind(this)
    })

    let a = document.querySelector("form-component")
    do {
      a = a.parentElement
    } while (a.tagName != "DASHBOARD-COMPONENT")
    this.dashboard = a
  }

  onChange(e) {
    this.state[e.target.name] = e.target.value
  }
  async onSubmit(e) {
    e.preventDefault()
    addLead(this.state, () => {
      this.dashboard.updateLeads()
      this.form.reset()
      this.state.reset()
    })
  }
}

customElements.define("form-component", Form)

export { Form }
