const template = document.getElementById("t-header")

class Header extends HTMLElement {
  constructor() {
    super()
    this.appendChild(template.content.cloneNode(true))
  }
  connectedCallback() {}
}

customElements.define("header-component", Header)

export { Header }
